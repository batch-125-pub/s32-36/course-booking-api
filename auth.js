//we will use this for authentication
	//login
	//retrieve user details

//JSON Web Token
	//methods:
		//sign(data, secret, {options})
			//creates a token
			//options -> token can have an expiration
		//verify(token, secret, callback func())
			//checks if the token is present
			//callback function contains the logic
		//decode(token, {}).payload
			//interpret/decodes the created token

let jwt = require('jsonwebtoken');

let secret = "CourseBookingAPI";

//create tokens

module.exports.createAccessToken = (user) => {

	let data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});

}

//verify token

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		console.log(token);
		return jwt.verify(token, secret, (error, data)=>{
			if(error){
				return res.send({auth: "failed"})
			} else {
				next();
			}
		});
	}
}

//decode token

module.exports.decode = (token) => {
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.decode(token, {complete: true}).payload;
	}
}
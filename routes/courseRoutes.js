const express = require('express'); 
const router = express.Router(); 
let auth = require('./../auth');

//controllers
const courseController = require('./../controllers/courseControllers');

//retrieve all active courses
router.get('/active-courses', (req, res) => {
	courseController.getAllActiveCourses().then(result => res.send(result));
})

//retrieve all courses
router.get('/all-courses', auth.verify, (req, res)=>{
	courseController.getAllCourses().then(result => res.send(result));
})

//addCourse
router.post('/add-courses', auth.verify, (req, res) => {
	courseController.addCourse(req.body).then(result => res.send(result));
})

//get single course by name

router.get('/search-course', auth.verify, (req, res) => {
	//console.log('from router', req.body.name);
	courseController.searchCourse(req.body).then(result => res.send(result));
})

//get single course by id in params


router.get('/:courseId', auth.verify, (req, res) => {
	courseController.searchCourseById(req.params.courseId).then(result=> res.send(result));
})

//edit Course

router.put('/:courseId/edit', auth.verify, (req, res) => {
	//console.log(req.headers.authorization);
	//console.log(req.params.courseId);
	courseController.editCourse(req.params.courseId, req.body).then(result=>res.send(result));
})

router.put('/:courseId/archive', auth.verify, (req, res) => {
	courseController.archiveCourse(req.params.courseId).then(result => res.send(result));
})

router.put('/:courseId/unarchive', auth.verify, (req, res) => {
	courseController.unarchiveCourse(req.params.courseId).then(result => res.send(result));
})

router.delete('/:courseId/delete', auth.verify, (req, res) => {
	courseController.deleteCourse(req.params.courseId).then(result => res.send(result));
})

module.exports = router;
let express = require('express');

let router = express.Router();

let userController = require("./../controllers/userControllers");

let auth = require('./../auth');

//check if email exists
router.post("/checkEmail", (req, res)=>{
	userController.checkEmailExists(req.body).then(result=>res.send(result));
})


//user registration
router.post("/register", (req, res)=>{
	console.log('hello from reg');
	console.log('hello from reg:', req.body);
	userController.register(req.body).then(result=>res.send(result))
})

//login
router.post("/login", (req, res) => {
	userController.login(req.body).then(result=>res.send(result));
})

//retrieve a specific user
router.get("/details", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	userController.getProfile(userData.id).then(result=>{res.send(result)});
})

router.post('/enroll', auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}

	console.log(data);
	userController.enrollCourse(data).then(result=>
		res.send(result));
})

module.exports = router;
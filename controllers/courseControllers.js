const Course = require('./../models/Courses');

module.exports.getAllActiveCourses = () => {
	return Course.find({isActive: true}).then( result => {return result});
}

module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	let returnVal = newCourse.save().then((result, error)=>{
		if(error){
			return error;
		} else {
			//console.log(result);
			return true;
		}
	})

	return returnVal;
}

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {return result});
	//return returnVal;
}

//search a single course by name
module.exports.searchCourse = (reqBody) => {
	//console.log(reqBody.name);
	return Course.find({name: `/${reqBody.name}/i` }).then((result)=>{return result});
}

//search a single course by Id

module.exports.searchCourseById = (params) => {
	return Course.findById({_id: params}).then(result => {return result});
}

module.exports.editCourse = (params, reqBody) => {
	//alert('This is in edit course');
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Course.findByIdAndUpdate(params, updatedCourse, {new: true}).then((result, error) => {

		if(error){
			return error;
		} else {
			return result;
		}
	});

}

module.exports.archiveCourse = (params) => {

	let updatedActiveCourse = {
		isActive: false
	}
	return Course.findByIdAndUpdate(params, updatedActiveCourse, {new: true}).then((result, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}

module.exports.unarchiveCourse = (params) => {

	let updatedActiveCourse = {
		isActive: true
	}
	return Course.findByIdAndUpdate(params, updatedActiveCourse, {new: true}).then((result, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}

module.exports.deleteCourse = (params) => {

	return Course.findByIdAndDelete(params).then((result, error) => {
		if(error){
			return false;
		} else {
			return true; 
		}
	})
}
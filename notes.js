/*

Minimum Viable Product is a product that has enough features to be useful to its target market. It is used to validate a product idea at the onset........

Booking System MVP must have the ff features:
- User registration
- User authentication(login)
- Retrieval of authenticated user's details
- Course creation by an authenticated user
- Retrieval of Courses
- Course archiving by an authenticated user

Booking system api dependencies:
Besides express and mongoose...
- bycrpt - for hashing user passwords prior to storage
- cors - for allowing cross-origin resource sharing
- jsonwebtoken - for implementing JSON web tokens


- Once user identity has been proven, a JWT may be issued to the source of the request
- A JWT is an object that securely transmits information between parties in a compact and self-contained way.
-Transmitted information can be verified and trusted because a JWT is digitally signed using a secret.....
> https://jwt.io/

Anatomy of JWT
-Header
-Payload
-Signature

typical structure: xxxxx.yyyyyy.zzzzzz

*/